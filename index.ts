import { logger } from '@toegang-voor-iedereen/typescript-logger'
import { Worker } from './src/Worker.js'
import { WorkerConfig } from './src/types/WorkerConfig.js'
import { WorkerMessageHandler } from './src/types/WorkerMessageHandler.js'

try {
    if (!process.env.EXCHANGE) {
        throw new Error('No exchange prefix defined')
    }

    const config: WorkerConfig = {
        name: 'worker-local-dummy',
        amqpHost: process.env.AMQP_HOST ?? 'localhost',
        amqpPass: process.env.AMQP_PASS,
        amqpPort: process.env.AMQP_PORT ?? '5672',
        amqpProtocol: process.env.AMQP_PROTOCOL ?? 'amqp',
        amqpUser: process.env.AMQP_USER,
        exchangePrefix: process.env.EXCHANGE,
        workerInstanceName: process.env.HOSTNAME ?? '',
        prefetchLimit: 10,
        minioHost: process.env.MINIO_HOST ?? 'localhost',
        minioPort: parseInt(process.env.MINIO_PORT ?? '9000'),
        minioAccessKey: process.env.MINIO_ACCESS_KEY ?? 'test',
        minioSecretKey: process.env.MINIO_SECRET_KEY ?? 'test',
        minioOutputBucketName: process.env.MINIO_OUTPUT_BUCKET_NAME ?? 'output',
        minioDebugBucketName: process.env.MINIO_DEBUG_BUCKET_NAME ?? 'kimi-debug',
        minioUseSSL: JSON.parse(process.env.MINIO_USE_SSL ?? 'false') === true,
    }

    const handler: WorkerMessageHandler = async ({ job }) => {
        return {
            stringResult: 'application/pdf',
            success: true,
            confidence: 100,
        }
    }

    const worker = new Worker({
        logger,
        messageHandler: handler,
        config,
    })

    await worker.start()
} catch (error) {
    if (error instanceof Error) {
        // Do stuff with this error...
    }
    throw error
}
