## [7.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/7.0.1...7.0.2) (2025-01-23)


### Bug Fixes

* **deps:** update dependency @types/node to v22.10.9 ([824f4a7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/824f4a736026e1645028436e35b697b478c8be96))

## [7.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/7.0.0...7.0.1) (2025-01-23)


### Bug Fixes

* ecs compliant fields ([1feeb61](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/1feeb61f75577d696727a471eebb29d79c518cc8))

# [7.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/6.0.1...7.0.0) (2025-01-06)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.11 ([67c11fd](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/67c11fd3848fbf668819558d1fd07cbc0cd9471d))


### Features

* node 22 ([e806f9c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/e806f9c2a04de5e0d09a2551a030cc406cd59ad4))


### BREAKING CHANGES

* node 22 changes the way import assertions are handled

## [6.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/6.0.0...6.0.1) (2024-12-30)


### Bug Fixes

* **deps:** update dependency minio to v8.0.3 ([2e25e86](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/2e25e8645576acdc0ecfe6117000b9dd5593f7d6))
* **deps:** update dependency zod to v3.24.1 ([82aca44](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/82aca4490617be7343799403bd4a121d6cd2cf89))

# [6.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/5.1.0...6.0.0) (2024-12-11)


* feat!: replacing minioClient with storage proxy in worker job handler parameters ([552caf4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/552caf40961ecfda3259693f6015bcc922e46873))


### Bug Fixes

* actually export the IRemoteFileStorage class ([43be15a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/43be15a395e3ce0e97f5f98285834634e2f8143d))


### Features

* added RemoteFileStorage implementation for MinIO ([137584c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/137584cb4a633550e66557a5613132259a8f665a))
* from now on, WorkerHandler receives the RemoteFileStorage instead of a MinIO client reference ([4478a55](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/4478a55fa6f0123399daf48ad1eada263f30a9da))


### BREAKING CHANGES

* With this change, the MinIO client is no longer exposed to the implementing workers. A new implementation is needed.

# [5.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/5.0.0...5.1.0) (2024-12-05)


### Bug Fixes

* **deps:** update dependency amqplib to v0.10.5 ([deaabd4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/deaabd48951a83014d6b45f6b05a186c7a081cb8))


### Features

* added RemoteFileStorage implementation for MinIO ([137584c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/137584cb4a633550e66557a5613132259a8f665a))
* Minio 8 ([1e7678f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/1e7678fcf7c25397a6e0d517d9f1554705a27547))

# [5.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.1.0...5.0.0) (2024-11-19)


### Features

* pass minio client to worker message handler ([5841ead](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/5841eaddfb84f647d368a22ae7a1d4f7bf008b4c))


### BREAKING CHANGES

* WorkerMessageHandler now has an additional parameter named minioClient

# [4.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.9...4.1.0) (2024-11-19)


### Features

* added minio to baseworker ([1a3acd2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/1a3acd29018521305630bf562c9eca9893389268))
* publish job to minio ([cedbf0b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/cedbf0b4deaabbdd81c8c0cad9b5debaf42739ea))

## [4.0.9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.8...4.0.9) (2024-11-14)


### Bug Fixes

* removed large data objects from default logger ([dd92a31](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/dd92a31ce06c0c37d794d61a018859fdda26b4d6))

## [4.0.8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.7...4.0.8) (2024-08-07)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.10 ([63e85de](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/63e85de2e5ec6a024303690284dd9504b427b443))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.9 ([540fca6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/540fca6e0046a99edcd56fed961badd791db764a))

## [4.0.7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.6...4.0.7) (2024-07-04)


### Bug Fixes

* improve project description ([8c1f5ef](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/8c1f5efa4f5a69d81918d171591f28727267652e))

## [4.0.6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.5...4.0.6) (2024-06-26)


### Bug Fixes

* update README.md ([93db5d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/93db5d5d41936f6330b07676b9deb7e0a4c72a56))

## [4.0.5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.4...4.0.5) (2024-06-26)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.7 ([89c76c7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/89c76c7b3e6ab95f15b3d00fdb8a4804616222a0))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.8 ([ffb1b68](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/ffb1b685c3c2c3591c3d68f042c0e5a53b75f9d3))
* removed publish log ([31e6351](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/31e6351e16b275954c236461b7a96983ce74135d))

## [4.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.3...4.0.4) (2024-05-23)


### Bug Fixes

* removed handledMessage from log to prevent logging overflow ([eb7cb7d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/eb7cb7d7e3267d59420625971fe8cdb496ea244d))

## [4.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.2...4.0.3) (2024-05-23)


### Bug Fixes

* actually use the shortened messageContent in the json ([7d1136f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/7d1136f94393d22a8a97985fc6bf2f7fca06964c))

## [4.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.1...4.0.2) (2024-05-23)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.5 ([ae3d328](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/ae3d328844bfa76fbbae7569ddba7f18d2a8a282))
* **deps:** update dependency zod to v3.23.7 ([a3c34bc](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/a3c34bc07d7d125b6ef41083696cc520741da2af))
* **deps:** update dependency zod to v3.23.8 ([1ae0d8e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/1ae0d8e10919dbacd581be349da1b46f8c2b3e84))
* if message is > 255 bytes, display message length instead of contents ([cb5bde6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/cb5bde62030b20e69b3622cd23d6b4da9e228b6c))
* update heartbeat interval to 60s instead of 30s ([b56f2d7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/b56f2d738957c2318184004386cbc8c9993775b1))

## [4.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/4.0.0...4.0.1) (2024-05-06)


### Bug Fixes

* **deps:** update dependency zod to v3.23.6 ([feee914](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/feee914f2107bbbc3733e681844cc3d3b81522c4))
* update packages ([f024b8f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/f024b8f771466caa9c9e00be6fc8fbc087a33df0))

# [4.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/3.9.4...4.0.0) (2024-04-30)


### Bug Fixes

* **deps:** update dependency zod to v3.23.5 ([c58fe6e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/c58fe6ea889de5cd88bbcedb7de1bb3613562912))


### Features

* answer worker scout ([28c3e2d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/28c3e2dc48d1ae19ddc8e031a138e8f0bc5e691a))


### BREAKING CHANGES

* workers now use the health exchange (-workers) to report availability

## [3.9.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/3.9.3...3.9.4) (2024-04-25)


### Bug Fixes

* clean from build command ([220ce75](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/220ce754add3fdfd8107e8f778b9fa3067af46f0))
* **deps:** update dependency zod to v3.22.5 ([b31ed45](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/b31ed45a129356e12e2a331bc9cf2fe756ce25cb))
* **deps:** update dependency zod to v3.23.0 ([daebbd6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/daebbd6694dd151604369f35cfcab831c6d120c7))
* **deps:** update dependency zod to v3.23.3 ([4e75122](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/4e75122e2d64db306464c86f9fa2b84d6ae9889b))
* **deps:** update dependency zod to v3.23.4 ([827c35b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/827c35b22c5ec77f0b872dcef09d4b3cbe6f5dd4))

## [3.9.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.9.2...3.9.3) (2024-04-24)


### Bug Fixes

* force release ([ce9918a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/ce9918ab5c350ea2f8ee687ae8310fe0f1e3c61e))

## [3.9.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.9.1...v3.9.2) (2024-04-17)


### Bug Fixes

* **deps:** update dependency amqplib to v0.10.4 ([d3da210](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/d3da210fd2950480263d5560b7443501786dc3f4))

## [3.9.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.9.0...v3.9.1) (2024-03-28)


### Bug Fixes

* **readme:** use correct logger in example ([bbae5fa](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/bbae5fa036aa904520b20333c6013c7411976bf8))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.6.3...v3.7.0) (2024-03-05)


### Features

* use quorum queues to support requeue counting ([c51d1ff](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/c51d1ff81c068cab06adb831bb7c4794648f641f))

### [3.6.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.6.2...v3.6.3) (2024-01-11)

### [3.6.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.6.1...v3.6.2) (2024-01-11)


### Bug Fixes

* exit process on shutdown ([c9aa284](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/c9aa28445d8e6e58bcd55df92fce66e6e2ef3207))

### [3.6.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.6.0...v3.6.1) (2024-01-11)


### Bug Fixes

* actually execute sigint/sigterm listeners ([3fc0754](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/3fc0754a35ed6851aa6c742b6114b7739866d34a))

## [3.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.5.0...v3.6.0) (2023-11-06)


### Features

* reject on unpublished result ([2fece16](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/2fece16bd46bbc0d8d32b3f84d0350dd1e1c81e3))

## [3.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.7...v3.5.0) (2023-11-06)


### Features

* handle connection error ([3e0d874](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/3e0d8742bdd92b13e963bd670a5238b2bc36329d))

## [3.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.7...v3.4.0) (2023-11-06)


### Features

* handle connection error ([3e0d874](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/3e0d8742bdd92b13e963bd670a5238b2bc36329d))

### [3.3.7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.6...v3.3.7) (2023-11-06)

### [3.3.6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.5...v3.3.6) (2023-11-06)

### [3.3.5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.4...v3.3.5) (2023-11-06)

### [3.3.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.3...v3.3.4) (2023-11-06)

### [3.3.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.4.0...v3.3.3) (2023-09-25)

### [3.3.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.1...v3.3.2) (2023-09-22)

### [3.3.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.1...v3.3.2) (2023-09-22)

### [3.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.3.0...v3.3.1) (2023-09-21)

## [3.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.2.2...v3.3.0) (2023-09-21)


### Features

* add workerJobCompleted event ([cd75fbe](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/cd75fbe07f9874cc464a2a864d205c45b2fac4a7))

### [3.2.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.2.1...v3.2.2) (2023-09-20)

### [3.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.2.0...v3.2.1) (2023-09-20)


### Bug Fixes

* update logger ([515ce3e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/515ce3e7fe04ae37b373a9271aa3584c5c43e447))

## [3.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.1.0...v3.2.0) (2023-09-20)


### Features

* demote availability status logs to trace level ([728ed15](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/728ed15ff0e264011fb9f8ad53790b801e38a249))

## [3.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.0.0...v3.1.0) (2023-09-19)


### Features

* use logger package ([82b3055](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/82b3055b7c857846e4b273478930259669d8b446))

### [3.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v3.0.0...v3.0.1) (2023-09-19)

## [3.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.3.1...v3.0.0) (2023-09-19)


### ⚠ BREAKING CHANGES

* **workerresults:** No longer supporting `result` as worker result attribute

### Bug Fixes

* **workerresults:** using new typed worker results ([ff13da5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/ff13da5e7e2f0a6dc3b73a3c47444f6f091b487a))

### [2.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.3.0...v2.3.1) (2023-09-14)

## [2.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.1.2...v2.3.0) (2023-09-14)


### Features

* add junit.xml report ([e7d4a7e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/e7d4a7e2f6038722392fc50ab46290f5cf8e50e7))
* expose logger in handler ([c8b41b5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/c8b41b57c832cb00b1740576fb73bae9fccaa64c))
* replace jest with vitest ([5f7f438](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/5f7f438a99fedd30f5fa5563ec9ad93c46afb48a))

### [2.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.2.0...v2.2.1) (2023-09-13)

## [2.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.1.2...v2.2.0) (2023-09-13)


### Features

* replace jest with vitest ([6d1e6a1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/6d1e6a18dace2d4eef388c4a0d69ba7f8dd9588e))

### [2.1.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.1.1...v2.1.2) (2023-09-12)

### [2.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.1.0...v2.1.1) (2023-09-12)

## [2.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v2.0.0...v2.1.0) (2023-09-12)


### Features

* export from root ([4d2bb8c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/4d2bb8cf6b7340d742263b4dea48f3eff45e1d0e))

## [2.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.5.0...v2.0.0) (2023-09-12)


### ⚠ BREAKING CHANGES

* **traceid:** The traceId is no longer in the body of the message, but in header x-trace-id

### Features

* **traceid:** introduce x-trace-id and job headers ([3d73cc4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/3d73cc4c7effcae3a9ff895eed9306bd35a5f2fe))

## [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.4.0...v1.5.0) (2023-09-08)


### Features

* optional filename and bucketname ([3fde7f3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/3fde7f3a784349220c73f924fa90df97a0c64b41))

## [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.3.0...v1.4.0) (2023-09-08)


### Features

* split off worker handler results ([e8164ca](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/e8164ca462a1704b6f160d7ac210e50be227f926))

## 1.3.0 (2023-09-08)


### Features

* add FileWorkerResult ([782eaa4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/782eaa418f46512bdd7e6323f285cdb2109e0fb2))
* package ([b2d8d02](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/b2d8d027fca1f4c0d37c4da4fc80d191c20393cc))

## 1.2.0 (2023-09-08)


### Features

* add FileWorkerResult ([782eaa4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/782eaa418f46512bdd7e6323f285cdb2109e0fb2))
* package ([b2d8d02](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/b2d8d027fca1f4c0d37c4da4fc80d191c20393cc))

### [1.1.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.1.1...v1.1.2) (2023-09-08)

### [1.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.1.0...v1.1.1) (2023-09-08)

## [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.0.4...v1.1.0) (2023-09-08)


### Features

* release step ([228d61e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/commit/228d61ebe4e0884989cb9a3cd915e998ad313b8c))

### [1.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.0.3...v1.0.4) (2023-09-07)

### [1.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.0.2...v1.0.3) (2023-09-07)

### [1.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/typescript/compare/v1.0.1...v1.0.2) (2023-09-07)

### 1.0.1 (2023-09-07)
