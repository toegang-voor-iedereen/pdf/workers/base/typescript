# PDF Processor Base Worker for Typescript

This repository contains a foundational worker implementation for Typescript projects. The worker handles connections to AMQP, you just have to add a message handler!


## How to use

Add an .npmrc file to your project containing:

```
@toegang-voor-iedereen:registry=https://gitlab.com/api/v4/groups/60879101/-/packages/npm
```

Then install this package by running: `npm install @toegang-voor-iedereen/kimi-baseworker`

You can now import Worker in your project!

### Example

```
import { logger } from '@toegang-voor-iedereen/typescript-logger'
import { Worker, WorkerConfig } from '@toegang-voor-iedereen/kimi-baseworker'

try {
    if (!process.env.EXCHANGE) {
        throw new Error('No exchange prefix defined')
    }

    const config: WorkerConfig = {
        name: 'your-awesome-worker-name',
        amqpHost: process.env.AMQP_HOST ?? 'localhost',
        amqpPass: process.env.AMQP_PASS,
        amqpPort: process.env.AMQP_PORT ?? '5672',
        amqpProtocol: process.env.AMQP_PROTOCOL ?? 'amqp',
        amqpUser: process.env.AMQP_USER,
        exchangePrefix: process.env.EXCHANGE,
        workerInstanceName: process.env.HOSTNAME ?? '',
        prefetchLimit: 10,
    }

    const handler: WorkerMessageHandler = async ({ job, storage }) => {
        // TODO: Get the actual thing you need.
        storage.getFileBuffer({ bucketName: job.bucketName, filePath: job.filePath })

        // TODO: Do the magic your worker does

        return {
            success: true,
            result: 'your amazing result',
            confidence: 100,
        }
    }

    const worker = new Worker({
        logger,
        messageHandler: handler,
        config,
    })

    await worker.start()
} catch (error) {
    if (error instanceof Error) {
        // Do stuff with this error...
    }
    throw error
}
```