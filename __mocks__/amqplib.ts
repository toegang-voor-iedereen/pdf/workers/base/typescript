import { vi } from 'vitest'
export const connect = vi.fn(async () => Promise.resolve(connection))

const connection = {
    createChannel: vi.fn(async () => Promise.resolve(channel)),
    once: vi.fn(),
    on: vi.fn(),
    close: vi.fn(),
}

const channel = {
    assertExchange: vi.fn(),
    assertQueue: vi.fn(async (stationQueueName: string) =>
        Promise.resolve({ queue: stationQueueName })
    ),
    bindQueue: vi.fn(),
    unbindQueue: vi.fn(),
    consume: vi.fn(),
    publish: vi.fn(),
    prefetch: vi.fn(),
    once: vi.fn(),
    on: vi.fn(),
    ack: vi.fn(),
    nack: vi.fn(),
}
