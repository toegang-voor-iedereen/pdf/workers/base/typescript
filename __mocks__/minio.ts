import { vi } from 'vitest'

export interface ItemBucketMetadata {
    [key: string]: any
}

export interface UploadedObjectInfo {
    etag: string
    versionId: string | null
}

export class Client {
    bucketExists: (name: string) => Promise<boolean>
    putObject: (
        bucketName: string,
        objectName: string,
        stream: ReadableStream | Buffer | string,
        metaData?: ItemBucketMetadata
    ) => Promise<UploadedObjectInfo>

    constructor() {
        this.bucketExists = vi.fn(async () => Promise.resolve(true))
        this.putObject = vi.fn(async () => Promise.resolve({ etag: 'abc', versionId: '1' }))
    }
}
