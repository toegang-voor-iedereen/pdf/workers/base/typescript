import { Client as MinioClient } from 'minio'
import { RemoteFileAttributes, RemoteFilePath } from './types/IRemoteFileStorage.js'

export class MinioRemoteFileStorage {
    #minioClient: MinioClient

    constructor({ minio }: { minio: MinioClient }) {
        this.#minioClient = minio
    }

    async putObjectJson({
        object,
        bucketName,
        filePath,
        encoding,
        metadata,
    }: {
        object: object
    } & RemoteFileAttributes): Promise<string> {
        return this.putObjectString({
            bucketName,
            filePath,
            stringValue: JSON.stringify(object),
            encoding,
            metadata,
        })
    }

    async putObjectString({
        stringValue,
        bucketName,
        filePath,
        encoding,
        metadata,
    }: {
        stringValue: string
    } & RemoteFileAttributes): Promise<string> {
        return this.putObjectBuffer({
            buffer: Buffer.from(stringValue, encoding),
            bucketName,
            filePath,
            metadata,
            encoding: 'binary',
        })
    }

    async putObjectBuffer({
        buffer,
        bucketName,
        filePath,
        metadata,
    }: {
        buffer: Buffer
    } & RemoteFileAttributes): Promise<string> {
        const { etag, versionId } = await this.#minioClient.putObject(
            bucketName,
            filePath,
            buffer,
            undefined,
            metadata
        )
        return etag
    }

    async getFileBuffer({ bucketName, filePath }: RemoteFilePath): Promise<Buffer> {
        const dataStream = await this.#minioClient.getObject(bucketName, filePath)
        return new Promise((resolve: (value: Buffer) => void, reject: (reason: Error) => void) => {
            const dataChunks: Buffer[] = []

            dataStream.on('data', (chunk: Buffer) => {
                dataChunks.push(chunk)
            })

            dataStream.on('end', () => {
                resolve(Buffer.concat(dataChunks))
            })

            dataStream.on('error', (error: Error) => {
                reject(error)
            })
        })
    }
}
