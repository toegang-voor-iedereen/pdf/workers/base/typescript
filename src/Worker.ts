import * as packageJson from '#package.json' with { type: 'json' }
import { Logger, TraceArgs, kimiEvent } from '@toegang-voor-iedereen/typescript-logger'
import { Channel, Connection, ConsumeMessage, connect } from 'amqplib'
import { Client as MinioClient } from 'minio'
import { createHash, randomUUID } from 'node:crypto'
import {
    ConnectionError,
    ExternalFileStorageNotReadyError,
    InvalidJob,
    InvalidWorkerScout,
    WorkerError,
} from './errors.js'
import { MinioRemoteFileStorage } from './MinioRemoteFileStorage.js'
import { WorkerAvailabilityStatus } from './types/WorkerAvailability.js'
import { WorkerConfig } from './types/WorkerConfig.js'
import { WorkerJob } from './types/WorkerJob.js'
import { WorkerMessageHandler } from './types/WorkerMessageHandler.js'
import { WorkerMessageHandlerResult, WorkerResult } from './types/WorkerResult.js'
import { WorkerScout } from './types/WorkerScout.js'
import { sleep } from './utils/index.js'

export class Worker {
    public amqpChannel: Channel | undefined

    private amqpClient: Connection | undefined
    private amqpHost: string
    private amqpPass: string | undefined
    private amqpPort: string
    private amqpProtocol: string
    private amqpUser: string | undefined
    private availabilityInterval: NodeJS.Timeout | undefined
    private exchangeHealth: string
    private exchangeJobs: string
    private exchangePrefix: string | undefined
    private exchangeResults: string
    private jobAckRoutingKeyPrefix = `ack.`
    private jobPattern = 'job.*'
    private jobResultRoutingKeyPrefix = 'result'
    private stationWorkerScoutRoutingKey = 'station.workers'

    private minioHost: string | undefined
    private minioPort: number | undefined
    private minioAccessKey: string | undefined
    private minioSecretKey: string | undefined
    private minioOutputBucketName: string | undefined
    private minioDebugBucketName: string | undefined
    private minioUseSSL = false
    private minioClient: MinioClient | undefined

    private logger: Logger
    private messageHandler: WorkerMessageHandler
    private prefetchLimit: number
    private queue: string
    private inFlight = new Set<number>()
    private shuttingDown = false
    private workerInstanceName: string
    private workerName: string
    private workerType: string
    private instanceTraceId: string
    private heartbeatIntervalSeconds = 60

    constructor({
        logger,
        messageHandler,
        config,
        traceId,
    }: {
        logger: Logger
        messageHandler: WorkerMessageHandler
        config: WorkerConfig
        traceId?: string
    }) {
        // Worker
        this.workerInstanceName = config.workerInstanceName
        this.workerName = config.name
        this.workerType = config.exchangePrefix

        // AMQP config
        this.amqpHost = config.amqpHost
        this.amqpPass = config.amqpPass
        this.amqpPort = config.amqpPort
        this.amqpProtocol = config.amqpProtocol
        this.amqpUser = config.amqpUser
        this.prefetchLimit = config.prefetchLimit ?? 10

        this.minioHost = config.minioHost
        this.minioPort = config.minioPort
        this.minioAccessKey = config.minioAccessKey
        this.minioSecretKey = config.minioSecretKey
        this.minioOutputBucketName = config.minioOutputBucketName
        this.minioDebugBucketName = config.minioDebugBucketName
        this.minioUseSSL = config.minioUseSSL ?? false

        // Exchange
        this.exchangePrefix = config.exchangePrefix
        this.exchangeJobs = `${this.exchangePrefix}-jobs`
        this.exchangeHealth = `${this.exchangePrefix}-workers`
        this.exchangeResults = `${this.exchangePrefix}-job-results`

        // Queue
        this.queue = this.workerName

        // Logging
        this.instanceTraceId = traceId ?? randomUUID()
        this.logger = logger.child({
            trace: {
                id: this.instanceTraceId,
            },
            worker: {
                name: this.workerName,
                type: this.workerType,
                instanceName: this.workerInstanceName,
                baseWorkerVersion: packageJson.default.version,
            },
        })

        if (this.workerName.substring(0, 7) !== 'worker-') {
            this.logger.warn(
                `Worker name "${this.workerName}" does not implement naming convention. Please prefix the workerName with "worker-"`
            )
        }

        // Message handling
        this.messageHandler = messageHandler
    }

    getInfo() {
        return {
            amqpHost: this.amqpHost,
            amqpPort: this.amqpPort,
            amqpProtocol: this.amqpProtocol,
            prefetchLimit: this.prefetchLimit,
            exchangeJobs: this.exchangeJobs,
            exchangeHealth: this.exchangeHealth,
            exchangeResults: this.exchangeResults,
            queue: this.queue,
            workerInstance: this.workerInstanceName,
            workerName: this.workerName,
            workerType: this.workerType,
        }
    }

    async start() {
        this.attachExternalEventListeners()
        await this.connectMinio()
        await this.connect()
        await this.assertChannel()
        await this.assertExchanges()
        await this.assertQueue()
        await this.bindQueues()

        const availabilityTraceArgs: TraceArgs = {
            traceId: this.instanceTraceId,
            logger: this.logger.child({ trace: { id: this.instanceTraceId } }),
        }
        this.sendAvailability({
            state: WorkerAvailabilityStatus.enum.Active,
            reason: 'Worker started',
            ...availabilityTraceArgs,
        })
        this.logger.info(
            `Starting heartbeat interval, every ${this.heartbeatIntervalSeconds} seconds.`
        )
        this.availabilityInterval = setInterval(() => {
            this.heartbeat(availabilityTraceArgs)
        }, this.heartbeatIntervalSeconds * 1000)
        await this.consumeChannel()
    }

    public async restart() {
        clearInterval(this.availabilityInterval)
        delete this.amqpClient
        delete this.amqpChannel
        return this.start()
    }

    private attachExternalEventListeners() {
        this.logger.info('Attach SIGTERM and SIGINT listeners')
        process.once('SIGTERM', this.onTerminationSignal)
        process.once('SIGINT', this.onTerminationSignal)
    }

    private onTerminationSignal: NodeJS.SignalsListener = async (signal) => {
        this.logger.info(`${signal} signal received.`)
        return this.shutdown()
    }

    private async inFlightDrained() {
        while (this.inFlight.size > 0) {
            this.logger.info(`There are ${this.inFlight.size} messages in flight...`)
            await sleep(200)
        }
    }

    public shutdown = async () => {
        this.logger.info(`Gracefully shutting down ${this.workerName}`)
        this.logger.info(`Shutting down connection...`)
        this.shuttingDown = true
        clearInterval(this.availabilityInterval)
        this.logger.info(`Setting channel prefetch to 0`)
        await this.amqpChannel?.prefetch(0, false)

        await this.inFlightDrained()

        this.logger.info('Closing AMQP client connection')
        await this.amqpClient?.close()

        this.logger.info(`Shutting down!`)
        this.logger.flush()
        await sleep(1000)
        process.exit()
    }

    private async connectMinio(): Promise<boolean> {
        if (
            this.minioHost === undefined ||
            this.minioAccessKey === undefined ||
            this.minioOutputBucketName === undefined
        ) {
            const debugObject = {
                minioHost: this.minioHost,
                minioAccessKey: this.minioAccessKey,
                minioOutputBucketName: this.minioOutputBucketName,
            }
            throw new ConnectionError(
                `MinIO failed to connect, as one of the connection parameters is empty: ${JSON.stringify(debugObject)}`
            )
        }
        if (this.minioSecretKey === undefined) {
            throw new ConnectionError(
                'MinIO failed to connect, as the minioSecretKey is not defined'
            )
        }

        this.minioClient = new MinioClient({
            endPoint: this.minioHost,
            port: this.minioPort,
            useSSL: this.minioUseSSL,
            accessKey: this.minioAccessKey,
            secretKey: this.minioSecretKey,
        })

        return this.minioClient.bucketExists(this.minioOutputBucketName)
    }

    private async connect() {
        this.logger.info(
            `Connecting to host: ${this.amqpHost} (${this.amqpProtocol}://${this.amqpUser}@${this.amqpHost}:${this.amqpPort})`
        )

        let connectionRetries = 0
        while (!this.shuttingDown && !this.amqpClient) {
            try {
                this.amqpClient = await connect(
                    `${this.amqpProtocol}://${
                        this.amqpUser && this.amqpPass ? `${this.amqpUser}:${this.amqpPass}@` : ''
                    }${this.amqpHost}:${this.amqpPort}`
                )
            } catch (err) {
                connectionRetries++
                this.logger[connectionRetries > 30 ? 'warn' : 'debug'](
                    `Failed to connect... Retrying (${connectionRetries})`
                )
                await sleep(2000)
            }
        }

        this.amqpClient?.on('error', () => this.handleConnectionError)
        this.amqpClient?.on('close', () => this.handleConnectionClose)

        this.logger.info(
            `Connected to host: ${this.amqpHost} (${this.amqpProtocol}://${this.amqpHost}:${this.amqpPort})`
        )
    }

    private async assertChannel() {
        this.logger.info(`Assert channel`)
        this.amqpChannel = await this.amqpClient?.createChannel()
        await this.amqpChannel?.prefetch(this.prefetchLimit, false)
        this.amqpChannel?.on('error', () => this.handleConnectionError)
        this.amqpChannel?.on('close', () => this.handleConnectionClose)
        this.logger.info(`Asserted channel`)
    }

    private async handleConnectionError() {
        this.logger.warn('A connection error occurred. Restarting connection...')
        void this.restart()
    }

    private async handleConnectionClose() {
        if (this.shuttingDown) {
            return
        }
        this.logger.warn('A connection was unexpectedly closed. Restarting connection...')
        void this.restart()
    }

    private async assertExchanges() {
        return Promise.all([
            this.assertExchange(this.exchangeHealth),
            this.assertExchange(this.exchangeJobs),
            this.assertExchange(this.exchangeResults),
        ])
    }

    private async assertExchange(exchangeName: string) {
        this.logger.info(`Assert exchange: ${exchangeName}`)
        await this.amqpChannel?.assertExchange(exchangeName, 'fanout', {
            durable: true,
        })
        this.logger.info(`Asserted exchange: ${exchangeName}`)
    }

    private async assertQueue() {
        this.logger.info(`Assert queue: ${this.queue}`)
        await this.amqpChannel?.assertQueue(this.queue, {
            exclusive: false,
            arguments: {
                'x-queue-type': 'quorum',
            },
        })
        this.logger.info(`Asserted queue: ${this.queue}`)
    }

    private async bindQueues() {
        return Promise.all([
            this.bindQueue(this.exchangeJobs, this.jobPattern),
            this.bindQueue(this.exchangeHealth, this.stationWorkerScoutRoutingKey),
        ])
    }

    private async bindQueue(exchangeName: string, routingKey: string) {
        this.logger.info(
            `Binding queue ${this.queue} to exchange ${exchangeName} with pattern ${routingKey}`
        )
        await this.amqpChannel?.bindQueue(this.queue, exchangeName, routingKey)
        this.logger.info(`Bound queue ${this.queue} to exchange ${exchangeName}`)
    }

    private publish({
        exchange,
        routingKey,
        body,
        logger,
        traceId,
        logLevel = 'info',
    }: {
        exchange: string
        routingKey: string
        body: Record<string, unknown>
        logLevel?: 'info' | 'trace'
    } & TraceArgs) {
        if (!this.amqpChannel) {
            logger.warn('Could not publish message, channel not available')
            return
        }

        try {
            return this.amqpChannel.publish(
                exchange,
                routingKey,
                Buffer.from(JSON.stringify(body), 'utf8'),
                {
                    headers: {
                        'x-kimi-worker-type': this.workerType,
                        'x-kimi-worker-name': this.workerName,
                        'x-kimi-worker-instance-name': this.workerInstanceName,
                        'x-trace-id': traceId,
                    },
                    contentType: 'application/json',
                    contentEncoding: 'utf8',
                }
            )
        } catch (err) {
            if (err instanceof Error && err.message.includes('Channel closed')) {
                void this.handleConnectionError()
                return false
            } else {
                throw err
            }
        }
    }

    private sendAvailability({
        state,
        reason,
        logger,
        traceId,
    }: {
        state: WorkerAvailabilityStatus
        reason: string
    } & TraceArgs) {
        const routingKey = `worker.${this.workerName}.${this.workerInstanceName}`
        logger.trace(`Sending state: ${state} (${reason}) -> ${this.exchangeResults}/${routingKey}`)

        const body = {
            workerType: this.workerType,
            workerName: this.workerName,
            workerInstance: this.workerInstanceName,
            state,
            reason,
            timestamp: Date.now().toString(),
        }

        const success = this.publish({
            exchange: this.exchangeResults,
            routingKey,
            body,
            traceId,
            logger,
            logLevel: 'trace',
        })
        if (!success) {
            logger.warn('Failed to send worker availability status')
        }
    }

    private heartbeat({ traceId, logger }: TraceArgs) {
        this.sendAvailability({
            state: WorkerAvailabilityStatus.enum.Active,
            reason: 'heartbeat',
            traceId,
            logger,
        })
    }

    private sendJobAck({
        traceId,
        recordId,
        jobId,
        logger,
    }: {
        recordId: string
        jobId: string
    } & TraceArgs) {
        logger.info(`Sending job ack`)

        const body = {
            traceId,
            recordId,
            jobId,
        }
        const routingKey = `${this.jobAckRoutingKeyPrefix}${jobId}`
        const success = this.publish({
            exchange: this.exchangeResults,
            routingKey,
            body,
            logger,
            traceId,
        })
        if (!success) {
            logger.warn('Failed to send worker job ack')
        }
    }

    private async checkJobMessageRoutingKey(input: { msg: ConsumeMessage }) {
        const { msg } = input
        if (msg.fields.routingKey.split('.')[0] !== this.jobPattern.split('.')[0]) {
            throw new InvalidJob(
                `Received message that doesn't follow pattern "${this.jobPattern}": ${msg.fields.routingKey}`
            )
        }

        let messageContent = msg.content.toString()
        if (messageContent.length > 255) {
            messageContent = `<String(${messageContent.length} bytes)>`
        }

        this.logger
            ?.child({
                data: { fields: msg.fields },
            })
            .info('Incoming message')
        return input
    }

    private async parseJobMessage({ msg }: { msg: ConsumeMessage }) {
        const job = WorkerJob.safeParse(JSON.parse(msg.content.toString()))
        if (!job.success) {
            throw new InvalidJob('Job content could not be parsed', job.error)
        }

        const jobId = createHash('md5')
            .update(
                `${msg.properties.headers?.['x-trace-id']}${job.data.recordId}${this.workerName}`
            )
            .digest('hex')
        return { job: job.data, jobId }
    }

    private async publishResult({
        job,
        jobId,
        result,
        traceId,
        logger,
    }: {
        job: WorkerJob
        jobId: string
        result: WorkerMessageHandlerResult
    } & TraceArgs) {
        const routingKey = `${this.jobResultRoutingKeyPrefix}.${jobId}`
        const body: WorkerResult = {
            traceId,
            recordId: job.recordId,
            jobId,
            timestamp: new Date().toISOString(),
            ...result,
        }

        return this.publish({ exchange: this.exchangeResults, routingKey, body, traceId, logger })
    }

    private async consumeChannel() {
        this.logger.info(`Start consuming (${this.queue})`)
        await this.amqpChannel?.consume(
            this.queue,
            (msg: ConsumeMessage | null) => this.handleMessage(msg),
            {
                consumerTag: process.env.HOSTNAME,
            }
        )
    }

    private handleMessage(msg: ConsumeMessage | null) {
        const traceId: string = msg?.properties?.headers?.['x-trace-id'] ?? randomUUID()
        const stringContent = msg?.content.toString()
        let jsonContent: Record<string, unknown> | undefined
        let content: string | undefined = stringContent

        try {
            if (!stringContent) {
                throw new Error('String content is empty')
            }
            jsonContent = JSON.parse(stringContent)
            content = undefined
        } catch {
            this.logger.warn('Found non-json content in message')
            jsonContent = undefined
        }

        let logger = this.logger.child({
            trace: { id: traceId },
        })

        if (!msg) {
            logger.trace(`Received empty message...`)
            return
        } else if (this.shuttingDown) {
            logger.info(
                `Message incoming after shutdown signal, reschedule message on queue (nack: ${msg.fields.deliveryTag})`
            )
            return this.amqpChannel?.nack(msg, false, true)
        }

        void this.handleMessageAsync({ msg, jsonContent, logger, traceId })
    }

    private async handleMessageAsync({
        msg,
        jsonContent,
        traceId,
        logger,
    }: {
        msg: ConsumeMessage
        jsonContent: Record<string, unknown> | undefined
    } & TraceArgs) {
        return new Promise<void>(async (resolve, reject) => {
            try {
                this.inFlight.add(msg.fields.deliveryTag)
                if (msg.properties?.headers?.['x-kimi-worker-name'] === this.workerName) {
                    logger.trace(`Received message from self... Ignore that...`)
                    return resolve()
                }

                if (msg.fields.routingKey == this.stationWorkerScoutRoutingKey) {
                    const workerScoutParseResult = WorkerScout.safeParse(jsonContent)
                    if (workerScoutParseResult.success === false) {
                        return reject(
                            new InvalidWorkerScout(
                                'WorkerScout content could not be parsed',
                                workerScoutParseResult.error
                            )
                        )
                    }

                    this.sendAvailability({
                        state: WorkerAvailabilityStatus.enum.Active,
                        reason: `Answering Worker Scout: ${workerScoutParseResult.data.reason}`,
                        traceId,
                        logger,
                    })
                    return resolve()
                } else {
                    if (this.minioClient === undefined) {
                        throw new ExternalFileStorageNotReadyError(
                            'MinIO client not initialized when handling the job'
                        )
                    }

                    // Check if routing key matches with job
                    await this.checkJobMessageRoutingKey({ msg })

                    // Parse the job
                    const { job, jobId } = await this.parseJobMessage({ msg })

                    logger = logger.child({ jobId, recordId: job.recordId })

                    // Acknowledge the job
                    this.sendJobAck({
                        traceId,
                        recordId: job.recordId,
                        jobId,
                        logger,
                    })

                    await this.storeJobInMinio({ job, jobId, traceId, logger })

                    const jobLogger = logger.child({ data: { jobId } })
                    const startTime = new Date()

                    // Handle the message
                    const result = await this.messageHandler({
                        logger: jobLogger,
                        job,
                        jobId,
                        storage: new MinioRemoteFileStorage({ minio: this.minioClient }),
                    })

                    kimiEvent(jobLogger, {
                        name: 'workerJobCompleted',
                        eventStartTime: startTime,
                        eventTime: new Date(),
                    })

                    // Publish the handler result
                    const published = await this.publishResult({
                        job,
                        jobId,
                        result,
                        logger,
                        traceId,
                    })
                    if (published) {
                        return resolve()
                    } else {
                        return reject(new WorkerError('Handler result could not be published'))
                    }
                }
            } catch (error) {
                reject(error)
            }
        })
            .catch((error) => {
                if (error instanceof Error) {
                    logger.error(error)
                } else {
                    logger.error('An unknown error happened...')
                }
            })
            .finally(() => {
                this.amqpChannel?.ack(msg) // TODO: What should happen in case of an error?
                this.inFlight.delete(msg.fields.deliveryTag)
            })
    }

    private async storeJobInMinio({
        job,
        jobId,
        traceId,
        logger,
    }: {
        job: WorkerJob
        jobId: string
        traceId: string
        logger: Logger
    }): Promise<void> {
        const bucketName = this.minioDebugBucketName
        if (bucketName === undefined) return

        const folderName = job.recordId
        const fileName = `${jobId}.json`
        const objectName = `${folderName}/${fileName}`

        logger
            .child({
                bucket_name: bucketName,
                folder_name: folderName,
                file_name: fileName,
            })
            .debug('Storing job debug file in MinIO...')

        await this.minioClient?.putObject(
            bucketName,
            objectName,
            Buffer.from(JSON.stringify(job)),
            undefined,
            {
                jobId: jobId,
                recordId: job.recordId,
                worker: this.workerName,
                instance: this.instanceTraceId,
                traceId: traceId,
            }
        )
    }
}
