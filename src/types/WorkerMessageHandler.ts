import { Logger } from '@toegang-voor-iedereen/typescript-logger'
import { IRemoteFileStorage } from './IRemoteFileStorage.js'
import { WorkerJob } from './WorkerJob.js'
import { WorkerMessageHandlerResult } from './WorkerResult.js'

export type WorkerMessageHandlerInput = {
    logger: Logger
    job: WorkerJob
    jobId: string
    storage: IRemoteFileStorage
}
export type WorkerMessageHandler = (
    input: WorkerMessageHandlerInput
) => Promise<WorkerMessageHandlerResult>
