import z from 'zod'
export const WorkerConfig = z.object({
    name: z.string(),
    amqpHost: z.string(),
    amqpPass: z.string().optional(),
    amqpPort: z.string(),
    amqpProtocol: z.string(),
    amqpUser: z.string().optional(),
    exchangePrefix: z.string(),
    workerInstanceName: z.string(),
    prefetchLimit: z.number().optional(),
    minioHost: z.string().optional(),
    minioPort: z.number().optional(),
    minioAccessKey: z.string().optional(),
    minioSecretKey: z.string().optional(),
    minioOutputBucketName: z.string().optional(),
    minioDebugBucketName: z.string().optional(),
    minioUseSSL: z.boolean().optional(),
})
export type WorkerConfig = z.infer<typeof WorkerConfig>
