import z from 'zod'
export const WorkerJob = z.object({
    recordId: z.string(),
    bucketName: z.string().optional(),
    filename: z.string().optional(),
    attributes: z.object({}).catchall(z.any()),
})
export type WorkerJob = z.infer<typeof WorkerJob>

export const WorkerJobAck = z.object({
    recordId: z.string(),
    jobId: z.string(),
})
export type WorkerJobAck = z.infer<typeof WorkerJobAck>
