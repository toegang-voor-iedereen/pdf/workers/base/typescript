import z from 'zod'

enum WorkerAvailabilityStatusEnum {
    Active = 'active',
    Interrupted = 'interrupted',
    Stopped = 'stopped',
}
export const WorkerAvailabilityStatus = z.nativeEnum(WorkerAvailabilityStatusEnum)
export type WorkerAvailabilityStatus = z.infer<typeof WorkerAvailabilityStatus>

export const WorkerAvailability = z.object({
    workerType: z.string(),
    workerName: z.string(),
    workerInstance: z.string(),
    state: WorkerAvailabilityStatus,
    reason: z.string(),
    timestamp: z.string(),
})
export type WorkerAvailability = z.infer<typeof WorkerAvailability>