import z from 'zod'

export const WorkerScout = z.object({
    reason: z.string(),
    timestamp: z.string(),
})
export type WorkerScout = z.infer<typeof WorkerScout>
