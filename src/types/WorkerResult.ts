import z from 'zod'

const BaseWorkerResult = z.object({
    traceId: z.string(),
    recordId: z.string(),
    jobId: z.string(),
    timestamp: z.string().nullable(),
    success: z.boolean(),
    confidence: z.number(),
    error: z
        .object({
            code: z.number(),
            message: z.string(),
        })
        .optional()
        .nullable(),
})
type BaseWorkerResult = z.infer<typeof BaseWorkerResult>

export const AttributeWorkerResult = BaseWorkerResult.extend({
    stringResult: z.string().nullable(),
})
export type AttributeWorkerResult = z.infer<typeof AttributeWorkerResult>

export const AttributeWorkerHandlerResult = AttributeWorkerResult.omit({
    traceId: true,
    recordId: true,
    jobId: true,
    timestamp: true,
})
export type AttributeWorkerHandlerResult = z.infer<typeof AttributeWorkerHandlerResult>

export const PageWorkerPDFInfo = z.object({
    author: z.string().optional().nullable(),
    creator: z.string().optional().nullable(),
    producer: z.string().optional().nullable(),
    title: z.string().optional().nullable(),
    subject: z.string().optional().nullable(),
    creationDate: z.string().optional().nullable(),
    modificationDate: z.string().optional().nullable(),
    pdfVersion: z.string().optional().nullable(),
    pageSize: z.string().optional().nullable(),
    form: z.string().optional().nullable(),
    hasJavascript: z.boolean().optional().nullable(),
    isEncrypted: z.boolean().optional().nullable(),
})
export type PageWorkerPDFInfo = z.infer<typeof PageWorkerPDFInfo>

export const PageWorkerResult = BaseWorkerResult.extend({
    pageResult: z
        .object({
            pageCount: z.number(),
            bucketName: z.string(),
            bucketPath: z.string(),
            fileExtension: z.string(),
            pdfInfo: PageWorkerPDFInfo,
        })
        .nullable(),
})
export type PageWorkerResult = z.infer<typeof PageWorkerResult>

export const PageWorkerHandlerResult = PageWorkerResult.omit({
    traceId: true,
    recordId: true,
    jobId: true,
    timestamp: true,
})
export type PageWorkerHandlerResult = z.infer<typeof PageWorkerHandlerResult>

export const ElementWorkerResult = BaseWorkerResult.extend({
    elementResult: z
        .object({
            elementCount: z.number(),
            bucketName: z.string(),
            bucketPath: z.string(),
            fileExtension: z.string(),
        })
        .nullable(),
})
export type ElementWorkerResult = z.infer<typeof ElementWorkerResult>

export const ElementWorkerHandlerResult = ElementWorkerResult.omit({
    traceId: true,
    recordId: true,
    jobId: true,
    timestamp: true,
})
export type ElementWorkerHandlerResult = z.infer<typeof ElementWorkerHandlerResult>

export const FileWorkerResult = BaseWorkerResult.extend({
    fileResult: z
        .object({
            bucketName: z.string(),
            bucketPath: z.string(),
            fileExtension: z.string(),
        })
        .nullable(),
})
export type FileWorkerResult = z.infer<typeof FileWorkerResult>

export const FileWorkerHandlerResult = FileWorkerResult.omit({
    traceId: true,
    recordId: true,
    jobId: true,
    timestamp: true,
})
export type FileWorkerHandlerResult = z.infer<typeof FileWorkerHandlerResult>

export const WorkerResult = z.union([
    AttributeWorkerResult,
    PageWorkerResult,
    ElementWorkerResult,
    FileWorkerResult,
])
export type WorkerResult = z.infer<typeof WorkerResult>

export const WorkerMessageHandlerResult = z.union([
    AttributeWorkerHandlerResult,
    PageWorkerHandlerResult,
    ElementWorkerHandlerResult,
    FileWorkerHandlerResult,
])
export type WorkerMessageHandlerResult = z.infer<typeof WorkerMessageHandlerResult>
