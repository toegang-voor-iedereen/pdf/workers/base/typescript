export type RemoteFilePath = {
    bucketName: string
    filePath: string
}
export type RemoteFileAttributes = RemoteFilePath & {
    encoding: BufferEncoding
    metadata?: Record<string, any>
}

export interface IRemoteFileStorage {
    putObjectJson(
        parameters: {
            object: object
        } & RemoteFileAttributes
    ): Promise<string>

    putObjectString(
        parameters: {
            stringValue: string
        } & RemoteFileAttributes
    ): Promise<string>

    putObjectBuffer(
        parameters: {
            buffer: Buffer
        } & RemoteFileAttributes
    ): Promise<string>

    getFileBuffer(parameters: RemoteFilePath): Promise<Buffer>
}
