vi.mock('amqplib')
vi.mock('minio')
import { Logger, logger } from '@toegang-voor-iedereen/typescript-logger'
import { Channel, ConsumeMessage, MessageProperties } from 'amqplib'
import { Client } from 'minio'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { Worker } from './Worker.js'
import { WorkerConfig } from './types/WorkerConfig.js'
import { WorkerJob } from './types/WorkerJob.js'
import { sleep } from './utils/index.js'

describe('Worker', () => {
    let worker: Worker
    vi.spyOn(process, 'exit').mockImplementation(() => undefined as never)
    vi.spyOn(logger, 'child').mockImplementation(vi.fn(() => logger) as unknown as Logger['child'])

    const traceId = 'some-trace-id'
    beforeEach(() => {
        worker = new Worker({
            logger,
            config: WorkerConfig.parse({
                name: 'worker-foo-to-bar-converter',
                amqpHost: 'localhost',
                amqpPass: 'bar',
                amqpPort: '5672',
                amqpProtocol: 'amqp',
                amqpUser: 'foo',
                exchangePrefix: 'foo-to-bar',
                workerInstanceName: 'worker-foo-to-bar-converter-x',
                prefetchLimit: 10,
                minioHost: 'mock-host',
                minioPort: 1234,
                minioAccessKey: 'abcd',
                minioSecretKey: 'efgh',
                minioOutputBucketName: 'output',
                minioDebugBucketName: 'mock-debug',
                minioUseSSL: false,
            }),
            messageHandler: async () => {
                return {
                    success: true,
                    confidence: 100,
                    stringResult: '42',
                }
            },
        })
    })

    afterEach(async () => {
        vi.useFakeTimers()
        await Promise.all([worker.shutdown(), vi.runAllTimersAsync()])
        vi.useRealTimers()
    })

    it('should instantiate with the correct config', async () => {
        expect(worker.getInfo()).toMatchInlineSnapshot(`
            {
              "amqpHost": "localhost",
              "amqpPort": "5672",
              "amqpProtocol": "amqp",
              "exchangeHealth": "foo-to-bar-workers",
              "exchangeJobs": "foo-to-bar-jobs",
              "exchangeResults": "foo-to-bar-job-results",
              "prefetchLimit": 10,
              "queue": "worker-foo-to-bar-converter",
              "workerInstance": "worker-foo-to-bar-converter-x",
              "workerName": "worker-foo-to-bar-converter",
              "workerType": "foo-to-bar",
            }
        `)
    })

    it('should be able to start', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        expect(worker['amqpChannel']?.publish).toHaveBeenCalledWith(
            'foo-to-bar-job-results',
            'worker.worker-foo-to-bar-converter.worker-foo-to-bar-converter-x',
            expect.any(Buffer),
            {
                contentType: 'application/json',
                contentEncoding: 'utf8',
                headers: {
                    'x-kimi-worker-instance-name': 'worker-foo-to-bar-converter-x',
                    'x-kimi-worker-name': 'worker-foo-to-bar-converter',
                    'x-kimi-worker-type': 'foo-to-bar',
                    'x-trace-id': expect.any(String),
                },
            }
        )

        // Two exchanges, one for jobs, one for results
        expect(worker['amqpChannel']?.assertExchange).toHaveBeenCalledTimes(3)
        expect(worker['amqpChannel']?.assertQueue).toHaveBeenCalledTimes(1)
        expect(worker['amqpChannel']?.bindQueue).toHaveBeenCalledTimes(2)
    })

    it('should be able to restart', async () => {
        await expect(worker.start()).resolves.not.toThrow()

        expect(worker['amqpChannel']?.publish).toHaveBeenLastCalledWith(
            'foo-to-bar-job-results',
            'worker.worker-foo-to-bar-converter.worker-foo-to-bar-converter-x',
            expect.any(Buffer),
            {
                contentType: 'application/json',
                contentEncoding: 'utf8',
                headers: {
                    'x-kimi-worker-instance-name': 'worker-foo-to-bar-converter-x',
                    'x-kimi-worker-name': 'worker-foo-to-bar-converter',
                    'x-kimi-worker-type': 'foo-to-bar',
                    'x-trace-id': expect.any(String),
                },
            }
        )

        // Three exchanges, one for jobs, one for results, one for health
        expect(worker['amqpChannel']?.assertExchange).toHaveBeenCalledTimes(3)
        expect(worker['amqpChannel']?.assertQueue).toHaveBeenCalledTimes(1)
        expect(worker['amqpChannel']?.bindQueue).toHaveBeenCalledTimes(2)

        await expect(worker.restart()).resolves.not.toThrow()

        // Three exchanges, one for jobs, one for results, one for health
        expect(worker['amqpChannel']?.assertExchange).toHaveBeenCalledTimes(6)
        expect(worker['amqpChannel']?.assertQueue).toHaveBeenCalledTimes(2)
        expect(worker['amqpChannel']?.bindQueue).toHaveBeenCalledTimes(4)
    })

    it('can send a heartbeat', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')

        worker['heartbeat']({ traceId, logger })
        const exchange = channel.mock.calls[0][0]
        expect(exchange).toEqual('foo-to-bar-job-results')
        const routingKey = channel.mock.calls[0][1]
        expect(routingKey).toEqual(
            'worker.worker-foo-to-bar-converter.worker-foo-to-bar-converter-x'
        )
        const content = JSON.parse(channel.mock.calls[0][2].toString('utf-8'))
        expect(content).toEqual(
            expect.objectContaining({
                workerName: 'worker-foo-to-bar-converter',
                workerInstance: 'worker-foo-to-bar-converter-x',
                workerType: 'foo-to-bar',
                state: 'active',
                reason: 'heartbeat',
                timestamp: expect.any(String),
            })
        )
    })

    it('can send a job ack', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')

        worker['sendJobAck']({
            traceId: 'some-trace-id',
            recordId: 'some-record-id',
            jobId: 'some-job-id',
            logger,
        })

        const exchange = channel.mock.calls[0][0]
        expect(exchange).toEqual('foo-to-bar-job-results')
        const routingKey = channel.mock.calls[0][1]
        expect(routingKey).toEqual('ack.some-job-id')
        const content = JSON.parse(channel.mock.calls[0][2].toString('utf-8'))
        expect(content).toEqual({
            jobId: 'some-job-id',
            recordId: 'some-record-id',
            traceId: 'some-trace-id',
        })
    })

    it('checks a job messages routing key', async () => {
        await expect(worker.start()).resolves.not.toThrow()

        const msg: ConsumeMessage = {
            fields: { routingKey: 'job.valid' },
            content: Buffer.from('joehoe'),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage
        // Valid
        await expect(worker['checkJobMessageRoutingKey']({ msg })).resolves.not.toThrow()

        // Invalid
        await expect(
            worker['checkJobMessageRoutingKey']({
                msg: { ...msg, fields: { ...msg.fields, routingKey: 'foo.invalid' } },
            })
        ).rejects.toThrowError(`Received message that doesn't follow pattern "job.*": foo.invalid`)
    })

    it('responds with health on a worker scout', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const sendAvailabilitySpy = vi.spyOn(worker as any, 'sendAvailability')
        const sendJobAckSpy = vi.spyOn(worker as any, 'sendJobAck')
        expect(sendAvailabilitySpy).not.toHaveBeenCalled()
        expect(sendJobAckSpy).not.toHaveBeenCalled()

        const msg: ConsumeMessage = {
            fields: { routingKey: 'station.workers' },
            content: Buffer.from('{"reason":"unit testing","timestamp":""}'),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        await expect(
            worker['handleMessageAsync']({
                msg,
                jsonContent: { reason: 'unit testing', timestamp: '' },
                logger,
                traceId,
            })
        ).resolves.not.toThrow()

        expect(sendAvailabilitySpy).toHaveBeenCalled()
        expect(sendJobAckSpy).not.toHaveBeenCalled()
    })

    it('parses a job message', async () => {
        await expect(worker.start()).resolves.not.toThrow()

        const job: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }
        const msg: ConsumeMessage = {
            content: Buffer.from(JSON.stringify(job)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        const invalidMsg: ConsumeMessage = {
            content: Buffer.from(JSON.stringify({ ...job, recordId: undefined })),
        } as unknown as ConsumeMessage

        // Valid
        await expect(worker['parseJobMessage']({ msg })).resolves.toStrictEqual({
            job: {
                attributes: {
                    foo: 'bar',
                },
                bucketName: 'some-bucket-name',
                filename: 'some-file-name',
                recordId: 'some-record-id',
            },
            jobId: expect.any(String),
        })

        // Invalid
        await expect(worker['parseJobMessage']({ msg: invalidMsg })).rejects.toThrowError(
            `Job content could not be parsed`
        )
    })

    it('publishes a job result', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        const msg: ConsumeMessage = {
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        const { job, jobId } = await worker['parseJobMessage']({ msg })

        await expect(
            worker['publishResult']({
                job,
                jobId,
                result: { success: true, confidence: 100, stringResult: 'great success!' },
                logger,
                traceId,
            })
        ).resolves.not.toThrow()

        const exchange = channel.mock.calls[0][0]
        expect(exchange).toEqual('foo-to-bar-job-results')
        const routingKey = channel.mock.calls[0][1]
        expect(routingKey).toEqual(`result.${jobId}`)
        const content = JSON.parse(channel.mock.calls[0][2].toString('utf-8'))
        expect(content).toStrictEqual({
            confidence: 100,
            jobId,
            recordId: 'some-record-id',
            stringResult: 'great success!',
            success: true,
            traceId: 'some-trace-id',
            timestamp: expect.any(String),
        })
    })

    it('handles a message', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const publish = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')
        const minioPutObject = vi.spyOn(worker['minioClient'] as Client, 'putObject')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        const msg: ConsumeMessage = {
            fields: { routingKey: 'job.valid', deliveryTag: 1 },
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        expect(vi.fn(() => worker['handleMessage'](msg))).not.toThrow()
        await sleep(50)
        const now = Date.now()
        while (worker['inFlight'].size > 0 || Date.now() - now > 1000) {
            await sleep(50)
        }

        // Acked the message
        expect(publish.mock.calls.length).toEqual(2)
        let exchange = publish.mock.calls[0][0]
        expect(exchange).toEqual('foo-to-bar-job-results')
        let routingKey = publish.mock.calls[0][1]
        expect(routingKey).toEqual(expect.stringContaining(`ack.`))
        let content = JSON.parse(publish.mock.calls[0][2].toString('utf-8'))
        expect(content).toEqual({
            jobId: expect.any(String),
            recordId: 'some-record-id',
            traceId: 'some-trace-id',
        })

        expect(minioPutObject.mock.calls.length).toBe(1)
        expect(minioPutObject.mock.calls[0][0]).toEqual('mock-debug')
        expect(minioPutObject.mock.calls[0][1]).toMatch(
            new RegExp('|some-record-id/[a-f0-9]+.json|i')
        )
        expect(minioPutObject.mock.calls[0][4]).toMatchObject({
            instance: expect.any(String),
            jobId: expect.any(String),
            recordId: 'some-record-id',
            traceId: 'some-trace-id',
            worker: 'worker-foo-to-bar-converter',
        })

        // Published the result
        expect(publish.mock.calls.length).toEqual(2)
        exchange = publish.mock.calls[1][0]
        expect(exchange).toEqual('foo-to-bar-job-results')
        routingKey = publish.mock.calls[1][1]
        expect(routingKey).toEqual(expect.stringContaining(`result.`))
        content = JSON.parse(publish.mock.calls[1][2].toString('utf-8'))
        expect(content).toStrictEqual({
            confidence: 100,
            jobId: expect.any(String),
            recordId: 'some-record-id',
            stringResult: '42',
            success: true,
            traceId: 'some-trace-id',
            timestamp: expect.any(String),
        })
    })

    it('handles an empty message', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        expect(vi.fn(() => worker['handleMessage'](null))).not.toThrow()
    })

    it('nacks a message when already shutting down', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        worker['shuttingDown'] = true
        const nack = vi.spyOn(worker['amqpChannel'] as Channel, 'nack')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        const msg: ConsumeMessage = {
            fields: { routingKey: 'job.valid', deliveryTag: 1 },
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        expect(vi.fn(() => worker['handleMessage'](msg))).not.toThrow()
        expect(nack).toHaveBeenCalledWith(msg, false, true)
    })

    it('ignores a message when it is published by the same worker', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        const msg: ConsumeMessage = {
            fields: { routingKey: 'job.valid', deliveryTag: 1 },
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-kimi-worker-name': worker['workerName'],
                    'x-trace-id': 'some-trace-id',
                },
            },
        } as unknown as ConsumeMessage

        expect(vi.fn(() => worker['handleMessage'](msg))).not.toThrow()
        await sleep(50)
        const now = Date.now()
        while (worker['inFlight'].size > 0 || Date.now() - now > 1000) {
            await sleep(50)
        }
        expect(channel).not.toHaveBeenCalledTimes(3)
    })

    it('handles unexpected errors thrown inside message handlers', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')
        const sendJobAck = vi.spyOn(worker as any, 'sendJobAck')
        const loggerError = vi.spyOn(worker['logger'] as Logger, 'error')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        // We call with an invalid routing key, which will throw an error
        const msg: ConsumeMessage = {
            fields: { routingKey: 'foo.invalid', deliveryTag: 1 },
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        expect(vi.fn(() => worker['handleMessage'](msg))).not.toThrow()
        await sleep(50)
        const now = Date.now()
        while (worker['inFlight'].size > 0 || Date.now() - now > 1000) {
            await sleep(50)
        }
        expect(sendJobAck).not.toHaveBeenCalled()
        expect(channel).not.toHaveBeenCalledTimes(3)
        expect(loggerError.mock.calls[0][0]).toMatchInlineSnapshot(
            `[InvalidJob: Received message that doesn't follow pattern "job.*": foo.invalid]`
        )
    })

    it('handles unexpected errors that are not an instance of Error thrown inside message handlers', async () => {
        await expect(worker.start()).resolves.not.toThrow()
        const channel = vi.spyOn(worker['amqpChannel'] as Channel, 'publish')
        const sendJobAck = vi.spyOn(worker as any, 'sendJobAck')
        const checkJobMessageRoutingKey = vi.spyOn(worker as any, 'checkJobMessageRoutingKey')
        const loggerError = vi.spyOn(worker['logger'] as Logger, 'error')

        const workerJob: WorkerJob = {
            recordId: 'some-record-id',
            bucketName: 'some-bucket-name',
            filename: 'some-file-name',
            attributes: {
                foo: 'bar',
            },
        }

        const msg = {
            fields: { routingKey: 'job.valid', deliveryTag: 1 },
            content: Buffer.from(JSON.stringify(workerJob)),
            properties: {
                headers: {
                    'x-trace-id': traceId,
                },
            } as Partial<MessageProperties>,
        } as Partial<ConsumeMessage> as ConsumeMessage

        // We will throw a weird error that isn't even really an error
        checkJobMessageRoutingKey.mockImplementationOnce(() => {
            throw 'weird'
        })
        expect(vi.fn(() => worker['handleMessage'](msg))).not.toThrow()
        await sleep(50)
        const now = Date.now()
        while (worker['inFlight'].size > 0 || Date.now() - now > 1000) {
            await sleep(50)
        }
        expect(sendJobAck).not.toHaveBeenCalled()
        expect(channel).not.toHaveBeenCalledTimes(3)
        expect(loggerError.mock.calls[0][0]).toMatchInlineSnapshot(`"An unknown error happened..."`)
    })

    it('can wait until all in flight messages are processed', async () => {
        await expect(worker.start()).resolves.not.toThrow()

        // Let's pretend there is one message in flight
        worker['inFlight'] = new Set<number>().add(1)
        expect(worker['inFlight'].size).toEqual(1)

        const removeInFlight = async () => {
            await sleep(250)
            worker['inFlight'].delete(1)
        }

        await Promise.all([worker['inFlightDrained'](), removeInFlight()])
        expect(worker['inFlight'].size).toEqual(0)
    })
})
