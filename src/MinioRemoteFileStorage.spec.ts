import { Client as MinioClient } from 'minio'
import { PassThrough } from 'stream'
import { Mock } from 'vitest'
import { MinioRemoteFileStorage } from './MinioRemoteFileStorage.js'

describe('RemoteFileStorage', () => {
    let minioClientMock: MinioClient
    let remoteFileStorage: MinioRemoteFileStorage

    beforeEach(() => {
        // Mock the minioClient methods we'll use
        minioClientMock = {
            putObject: vi.fn(),
            getObject: vi.fn(),
        } as unknown as MinioClient

        remoteFileStorage = new MinioRemoteFileStorage({ minio: minioClientMock })
    })

    describe('putObjectJson', () => {
        it('should put a JSON object as a string and return the etag', async () => {
            const mockEtag = 'test-etag'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            const objectToStore = { foo: 'bar', nested: { key: 123 } }
            const result = await remoteFileStorage.putObjectJson({
                object: objectToStore,
                bucketName: 'my-bucket',
                filePath: 'folder/file.json',
                encoding: 'utf-8',
                metadata: { 'Content-Type': 'application/json' },
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            // Ensure the object was stringified
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[0]).toBe('my-bucket')
            expect(callArgs[1]).toBe('folder/file.json')
            expect(callArgs[2]).toBeInstanceOf(Buffer)
            expect(callArgs[4]).toEqual({ 'Content-Type': 'application/json' })
            expect(result).toBe(mockEtag)
        })

        it('should still work if metadata is not provided', async () => {
            const mockEtag = 'etag-without-metadata'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            const objectToStore = { foo: 'bar' }
            const result = await remoteFileStorage.putObjectJson({
                object: objectToStore,
                bucketName: 'my-bucket',
                filePath: 'no-metadata.json',
                encoding: 'utf-8',
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[4]).toBeUndefined()
            expect(result).toBe(mockEtag)
        })
    })

    describe('putObjectString', () => {
        it('should put a string and return the etag', async () => {
            const mockEtag = 'string-etag'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            const result = await remoteFileStorage.putObjectString({
                stringValue: 'hello world',
                bucketName: 'my-bucket',
                filePath: 'folder/hello.txt',
                encoding: 'utf-8',
                metadata: { foo: 'bar', baz: 123 },
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[0]).toBe('my-bucket')
            expect(callArgs[1]).toBe('folder/hello.txt')
            expect(callArgs[2]).toBeInstanceOf(Buffer)
            expect(callArgs[4]).toEqual({ foo: 'bar', baz: 123 })
            expect(result).toBe(mockEtag)
        })

        it('should default to binary encoding for the actual put', async () => {
            const mockEtag = 'binary-etag'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            await remoteFileStorage.putObjectString({
                stringValue: 'testing',
                bucketName: 'my-bucket',
                filePath: 'folder/test.txt',
                encoding: 'base64',
                metadata: { foo: 'bar', baz: 123 },
            })

            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[2]).toBeInstanceOf(Buffer)
        })

        it('should work without metadata', async () => {
            const mockEtag = 'no-metadata-etag'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            const result = await remoteFileStorage.putObjectString({
                stringValue: 'no metadata',
                bucketName: 'my-bucket',
                filePath: 'no-meta.txt',
                encoding: 'utf-8',
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[4]).toBeUndefined()
            expect(result).toBe(mockEtag)
        })
    })

    describe('putObjectBuffer', () => {
        it('should put a buffer and return the etag', async () => {
            const mockEtag = 'buffer-etag'
            const mockVersionId = 'some-version-id'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({
                etag: mockEtag,
                versionId: mockVersionId,
            })

            const buffer = Buffer.from('some binary content')
            const result = await remoteFileStorage.putObjectBuffer({
                buffer,
                bucketName: 'my-bucket',
                filePath: 'folder/binary.bin',
                encoding: 'binary',
                metadata: { 'X-Custom': 'Metadata' },
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[0]).toBe('my-bucket')
            expect(callArgs[1]).toBe('folder/binary.bin')
            expect(callArgs[2]).toBe(buffer)
            expect(callArgs[4]).toEqual({ 'X-Custom': 'Metadata' })
            expect(result).toBe(mockEtag)
        })

        it('should still work if metadata is not provided', async () => {
            const mockEtag = 'etag-no-meta-buffer'
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: mockEtag })

            const buffer = Buffer.from('no metadata')
            const result = await remoteFileStorage.putObjectBuffer({
                buffer,
                bucketName: 'my-bucket',
                filePath: 'no-meta.bin',
                encoding: 'binary',
            })

            expect(minioClientMock.putObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.putObject as Mock).mock.calls[0]
            expect(callArgs[4]).toBeUndefined()
            expect(result).toBe(mockEtag)
        })

        it('should handle errors thrown by minio client', async () => {
            ;(minioClientMock.putObject as Mock).mockRejectedValue(new Error('Minio error'))

            const buffer = Buffer.from('error test')
            await expect(
                remoteFileStorage.putObjectBuffer({
                    buffer,
                    bucketName: 'my-bucket',
                    filePath: 'error.bin',
                    encoding: 'binary',
                    metadata: { foo: 'bar', baz: 123 },
                })
            ).rejects.toThrow('Minio error')
        })
    })

    describe('getFileBuffer', () => {
        it('should return a buffer assembled from data chunks', async () => {
            const dataStream = new PassThrough()
            ;(minioClientMock.getObject as Mock).mockResolvedValue(dataStream)

            const dataChunks = ['Hello ', 'World', '!']
            // Push chunks into the stream asynchronously
            setTimeout(() => {
                dataChunks.forEach((chunk) => dataStream.push(Buffer.from(chunk)))
                dataStream.push(null)
            }, 10)

            const result = await remoteFileStorage.getFileBuffer({
                bucketName: 'my-bucket',
                filePath: 'folder/file.txt',
            })

            expect(minioClientMock.getObject).toHaveBeenCalledTimes(1)
            const callArgs = (minioClientMock.getObject as Mock).mock.calls[0]
            expect(callArgs[0]).toBe('my-bucket')
            expect(callArgs[1]).toBe('folder/file.txt')
            expect(result.toString()).toBe('Hello World!')
        })

        it('should handle stream errors', async () => {
            const dataStream = new PassThrough()
            ;(minioClientMock.getObject as Mock).mockResolvedValue(dataStream)

            setTimeout(() => {
                dataStream.emit('error', new Error('Stream error'))
            }, 10)

            await expect(
                remoteFileStorage.getFileBuffer({
                    bucketName: 'my-bucket',
                    filePath: 'error-file.txt',
                })
            ).rejects.toThrow('Stream error')
        })
    })

    describe('integration between methods', () => {
        it('putObjectJson should call putObjectString internally', async () => {
            const spy = vi.spyOn(remoteFileStorage as any, 'putObjectString')
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: 'json-etag' })

            await remoteFileStorage.putObjectJson({
                object: { key: 'value' },
                bucketName: 'my-bucket',
                filePath: 'test.json',
                encoding: 'utf-8',
                metadata: { foo: 'bar', baz: 123 },
            })

            expect(spy).toHaveBeenCalledTimes(1)
            expect(spy.mock.lastCall).toMatchInlineSnapshot(`
              [
                {
                  "bucketName": "my-bucket",
                  "encoding": "utf-8",
                  "filePath": "test.json",
                  "metadata": {
                    "baz": 123,
                    "foo": "bar",
                  },
                  "stringValue": "{"key":"value"}",
                },
              ]
            `)
        })

        it('putObjectString should call putObjectBuffer internally', async () => {
            const spy = vi.spyOn(remoteFileStorage as any, 'putObjectBuffer')
            ;(minioClientMock.putObject as Mock).mockResolvedValue({ etag: 'string-etag' })

            await remoteFileStorage.putObjectString({
                stringValue: 'test string with characters eauitm: éåüî™',
                bucketName: 'my-bucket',
                filePath: 'test.txt',
                encoding: 'utf-8',
                metadata: { foo: 'bar', baz: 123 },
            })

            expect(spy).toHaveBeenCalledTimes(1)
            const receivedBuffer = spy.mock.lastCall?.at(0)
            expect(receivedBuffer).toMatchInlineSnapshot(`
              {
                "bucketName": "my-bucket",
                "buffer": {
                  "data": [
                    116,
                    101,
                    115,
                    116,
                    32,
                    115,
                    116,
                    114,
                    105,
                    110,
                    103,
                    32,
                    119,
                    105,
                    116,
                    104,
                    32,
                    99,
                    104,
                    97,
                    114,
                    97,
                    99,
                    116,
                    101,
                    114,
                    115,
                    32,
                    101,
                    97,
                    117,
                    105,
                    116,
                    109,
                    58,
                    32,
                    195,
                    169,
                    195,
                    165,
                    195,
                    188,
                    195,
                    174,
                    226,
                    132,
                    162,
                  ],
                  "type": "Buffer",
                },
                "encoding": "binary",
                "filePath": "test.txt",
                "metadata": {
                  "baz": 123,
                  "foo": "bar",
                },
              }
            `)
        })
    })
})
