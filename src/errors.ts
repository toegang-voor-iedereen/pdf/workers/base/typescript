export class WorkerError extends Error {
    originalError: Error | undefined
    constructor(message?: string, originalError?: Error) {
        super(message)
        Object.setPrototypeOf(this, new.target.prototype)
        this.name = new.target.name
        this.originalError = originalError
    }
}

export class InvalidJob extends WorkerError {}
export class InvalidWorkerScout extends WorkerError {}
export class ConnectionError extends WorkerError {}
export class ExternalFileStorageNotReadyError extends WorkerError {}
